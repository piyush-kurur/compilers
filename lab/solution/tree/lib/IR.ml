
type binop = PLUS | MINUS | DIV | AND | OR | LSHIFT  | RSHIFT | ARSHIFT | XOR
type relop = EQ | NE | LT | GT | LE | GE | ULT | ULE | UGT | UGE

type exp = CONST of int
	 | NAME  of Label.t
	 | TEMP  of Temp.t
	 | BINOP of binop * exp * exp
	 | MEM   of exp
	 | CALL  of exp * exp list
	 | ESEQ  of stmt * exp


and stmt = MOVE of exp * exp
	 | EXP  of exp
	 | JUMP of exp * Label.t list
	 | CJUMP of relop * exp * exp * Label.t * Label.t
	 | SEQ of stmt * stmt
	 | LABEL of Label.t


(** {1 Canonisation starts here }

The canonical form of expression is such that

1. There are no ESEQ statements

2. All call are at the top level.


The canonisation of an expression takes a expression e as argument
and gives a pair of {[stmt * exp]} where the first component captures
the effect of evaluation the expression and the second component
is the canonical form of the expression.

 *)

(** {2 Type safety }

Canonisation procedure involves non-trivial manipulation of exp and
   stmt.  The module below essentially exposes the canonical form of
   {[expr]} and {[stmt]} by only exposing those constructors that
   preserve canonicity.

 *)

module Safe : sig
  type cexp
  type cstmt


  val const : int -> cexp
  val name  : Label.t -> cexp
  val temp  : Temp.t -> cexp
  val binop : binop -> cexp -> cexp -> cexp
  val mem   : cexp -> cexp
  val call  : cexp -> cexp list -> cexp
  val eseq  : cstmt -> cexp -> cstmt * cexp

  val move       : cexp -> cexp -> cstmt
  val expression : cexp -> cstmt
  val jump       : cexp -> Label.t list -> cstmt
  val cjump      : relop -> cexp * cexp -> Label.t * Label.t -> cstmt
  val seq        : cstmt -> cstmt -> cstmt
  val label      : Label.t -> cstmt

  val toExp   : cexp -> exp
  val toStmts : cstmt -> stmt list

  (* Commuting statments across expressions *)
  val commute : cexp -> cstmt -> cstmt * cexp
  val doNothing : cstmt
end = struct
  type cexp = exp
  type cstmt = stmt

  let doNothing = EXP (CONST 42)
  let toExp x = x
  let rec toStmts = function
      SEQ (s1,s2) -> toStmts s1 @ toStmts s2
    | s         -> [s]


  let const x = CONST x
  let name  x = NAME x
  let temp  x = TEMP x
  let binop oper e1 e2 = BINOP (oper, e1 , e2)
  let mem   e =  MEM e
  let call f args = CALL (f, args)


  let move x e     = MOVE (x,e)
  let expression x = EXP x
  let jump x ls    = JUMP (x, ls)
  let cjump rop es ls = let (e1,e2)  = es in
                        let (lT, lF) = ls in
                        CJUMP (rop,e1, e2, lT, lF)

  let seq  s sp = SEQ (s, sp)

  let label l = LABEL l

  let rec eseq s = function
      ESEQ (sp,e) -> let (spp, ep) = eseq sp e in
                     (seq s spp, ep)
    | e -> (s,e)

  let does_commute e = function
      EXP _ -> true
    | _     -> match e with
               | CONST _ -> true
               | NAME _ -> true
               | _ -> false


  let rec commute e s
    = if does_commute e s then (s,e)
      else match s with
             SEQ (s,ss) -> let (sp, ep) = commute e s in
                           let (ssp, epp) = commute ep ss in
                           (seq sp ssp, epp)
           | s -> let t        = temp (Temp.next ()) in
                  let saveEinT = move t e in
                  (seq  saveEinT s, t)

end

open Safe



(** {1 The canonisation algorithm }


Canonisation is performed by applying a set of rewrite rules. While
the rule itself is easy to explain, recursively applying the rewrite
rules means defining a set of  4 mutually recursive functions.

{[

val do_stmt : stmt -> cstmt
val do_exp  : exp -> cstmt * cexp
val reorder_exp : exp list -> eBuilder -> cstmt * cexp
val reorder_stmt : exp list -> sBuilder -> cstmt

]}

 *)

(**

Builders are functions that build a cnonical form of the
   expression/stmt from canonical form of sub-expressions. The
   {[do_*]} function should be seen as the one which extracts the
   sub-expressions (that need canonising) and supplies it to the
   {[reoder_*]} functions together with the associated builder. The
   {[reoder_*]} functions in-turn calls the builder on the cannonical
   form of its subexpressions. The canonical form is got by
   recursively calling the {[do_*]} functions.

 *)


type eBuilder = cexp list -> cexp
type sBuilder = cexp list -> cstmt

(**

Let us first define the builders. We only need them for those
constructors that have subexpressions.

 *)

exception BuildError of string

let buildError s = raise (BuildError s)

let b_BINOP oper : eBuilder = function
  | [e1; e2] -> binop oper e1 e2
  | _ -> buildError "BINOP"

let b_MEM : eBuilder = function
  | [e] -> mem e
  | _ -> buildError "MEM"

let b_CALL : eBuilder = function
  | f :: args -> call f args
  | _         -> buildError "CALL"

let b_MOVE : sBuilder = function
  | [e1 ; e2] -> move e1 e2
  | _ -> buildError "MOVE"

let b_EXP : sBuilder = function
  | [e] -> expression e
  | _ -> buildError "EXP"

let b_JUMP ls : sBuilder = function
  | [e] -> jump e ls
  | _   -> buildError "JUMP"

let b_CJUMP lT lF relop : sBuilder = function
  | [e1 ; e2] -> cjump relop (e1,e2) (lT, lF)
  | _ -> buildError "CJUMP"

exception NotImplemented
let notImplemented () = raise NotImplemented


let rec commutes  = function
  | []    -> (doNothing, [])
  | (s,e) :: rest -> let (srest, erest) = commutes rest in
                     let (sp, ep)       = commute e srest in
                     (seq s sp, ep :: erest)

let rec do_stmt = function
  | MOVE (e1, e2)  -> reorder_stmt [e1 ; e2] b_MOVE
  | EXP  e        -> reorder_stmt [e] b_EXP
  | JUMP (e , ls) -> reorder_stmt [e] (b_JUMP ls)
  | CJUMP (rop, e1, e2, lT, lF) -> reorder_stmt [e1; e2] (b_CJUMP lT lF rop)
  | SEQ (s1, s2) -> let cs1 = do_stmt s1 in
                    let cs2 = do_stmt s2 in
                    seq cs1 cs2
  | LABEL l -> label l

and do_exp = function
  | BINOP (oper, e1, e2) -> reorder_exp [e1; e2]   (b_BINOP oper)
  | MEM   e              -> reorder_exp [e]         b_MEM
  | CALL  (f , args)     -> reorder_exp (f :: args) b_CALL
  | CONST i -> (doNothing, const i)
  | NAME  l -> (doNothing, name l)
  | TEMP  t -> (doNothing, temp t)
  | ESEQ  (s , e)    -> let cs = do_stmt s in
                        let (csp, ce) = do_exp e in
                        (seq cs csp, ce)

and reorder_stmt es bfn =
  let ces = List.map do_exp es in
  let (s,es) = commutes ces in
  seq s (bfn es)


and reorder_exp es bfn =
  let ces    = List.map do_exp es in
  let (s,es) = commutes ces in
  (s, bfn es)
