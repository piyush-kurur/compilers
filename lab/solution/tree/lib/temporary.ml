type t = User of string | Temp of int
let nextRef = ref 0
let user x = User x
let next = function
    () -> let x = !nextRef
          in nextRef := (x + 1) ; Temp x

let to_string = function
    User x -> x
  | Temp x -> "%" ^ Int.to_string x
