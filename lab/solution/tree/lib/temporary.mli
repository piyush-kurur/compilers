type t
val user : string -> t
val next : unit -> t
val to_string : t -> string
