#+TITLE: Lab 4: For Loops (Deadline 10 March 2022).

The goal of this assignment is to support a simple iterative loop in
the language. The statements in the language, in addition to what was
present before now has for loops.

   #+begin_example
   program = list of <stmt>
   stmt =  <identifier> := <expresion>
        |  print <expression>
        |  for <identifier> = <const> to <const>
	   do
	      list of <stmt>
	   done
   #+end_example


   #+begin_example

   y := 2;
   x := 40 + y;      # Outer x
   print x + y;

   for x = 0 to 100  # inner x
   do
     print x         # this is inner x
   done;

   print x           # this is outer x (will print 42)

   #+end_example

* Additional Compiler data structures.

  There are two important changes that needs to be made in the code to make it
  possible to support for loops.

  - Labels :: Just like temporary variables captured by the ~Temp~, we
    now need a way to generate temporary labels. This is required so that the
    for loop can be converted into appropriate jump.

  - Local variables :: The looping variable (x in the example above)
    is local to the loop. When one comes out of the loop x has to be
    what it was before the entry of the loop.


** The ~Temps~ with labels.
#+lable:sec-temps

The ~Temp~ structure will need modification

#+begin_example sml
  signature TEMP =
    sig
       type label
       type temp
       val newlabel   : unit -> label
       val newtemp    : unit -> temp

       val labelToString : label -> string
       val tempToString : temp -> string

    end

  structure Temp :> TEMP = struct

     type label = int
     type temp  = int (* 2ʷ many variables on a w-sized machine *)
		      (* you can use IntInf.int if you want unbounded *)

    (* Complete this structure definition *)

  end
#+end_example

** Handling local variables.

The code generation requires one to maintain a map between program
variables and ~Temp.temp~. We can use ~Atom.atom~ for program
variables and ~Temp.temp AtomMap.map~ as the environment as in the
last assignment. The function application ~AtomMap.insert (m,k,v)~
does not mutate the map ~m~ input map, rather it gives a new map ~m'~
which is exactly the same as ~m~ but for the key ~k~. On key ~k~ the
map ~m'~ has value ~v~ stored. We can use this to maintain the stack
like allocation that we need for compiling loops (and other block
structures).

The idea is as follows. Assume that you have a function ~codeGen :
Env.t -> stmt -> IR.prog~. It will essentially take an element of the
~stmt~ type and pattern match on it and give out the necessary code.
You will have to use an updated environment when you are generating
code for the inner body of the loop.

#+begin_src sml

  type variable = Atom.atom

  datatype stmt =
	 ...
	 | FOR of variable * int * int * stmt list

    val codeGen     : Env.t -> stmt -> IR.prog
    val codeGenList : Env.t -> stmt list -> IR.prog
    val forLoop     : Env.t -> variable -> int ->  int -> stmt list -> IR.prog
    fun forLoop eta ident start stop body =
	let val t    = Temp.newtemp ()
	    val l    = Temp.newlabel ()
	    val etap = AtomMap.insert (eta, ident, t)
	in
	    codeGenList etap body
			(* something more required here *)
	end
    fun codeGen eta s = case s of
			    .. -> ..
			  | ... ->
			  | FOR (ident, start, stop, body) -> forLoop ident start stop body
    and codeGenList eta xs = ...

#+end_src
