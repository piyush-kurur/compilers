#+TITLE: Lab 1: Updating the reverse polish compiler

* Problem description  (Dead line Feb 7 Monday 23:55)


The reverse polish compiler only supports Plus (+) and Mul
(*). Furthermore it does not have bracketing --- you cannot give ~2 *
(3 + 4)~ as input to the compiler.


1. Extend the reverse polish compiler to support division

2. Extend the reverse polish compiler to support brackets.

You should have a separate directory inside your repository called
reverse-polish. Start by coping all the files in [[reverse-polish][../reverse-polish]] And
then commit it. This will be the starting version of your reverse
polish compiler. Then edit the repository to do the above tasks.


In each of the above tasks decide whether

1. The ast of expr language changes ?

2. The concrete syntax of the language change ?


| Particulars | Abstract Syntax | Concrete Syntax |
|-------------+-----------------+-----------------|
| division    | ✓               | ✓               |
| bracket     | ×               | ✓               |


** Changing abstract syntax

Abstract syntax trees are represented by a datatype in the
~ast.sml~. You will need to change that whenever the abstract syntax
of the language changes.

** Changing concrete syntax

There are two utilities that you need to know to write the front end
of your compiler.

- ~ml-yacc~ :: This takes the CFG corresponding to concrete syntax and
               writeout an sml file which contains the parser for your
               language.

- ~ml-lex~ :: This takes a set of regular expressions that describe your
	      language tokens and writes (generates an SML file) a token scanner.

* Experiments to be done in the lab.

1. Clone the compilers repository https://gitlab.com/piyush-kurur/compilers.git

2. Go to the reverse-polish directory and compile the code using make.

3. Change the expr.grm file by commenting out the lines %left (The
   comment syntax is the same as SML)

4. What happens if you interchange the %left PLUS MINUS like with the
   %left MUL line ?
