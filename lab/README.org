#+TITLE: Compilers Lab 2021 January Semester.
* Laboratory problems

The aim of this lab sessions is to get you familiarised with tools
that are required to build a compiler. The weekly set of assignments
culminate in a compiler that compiles programs written in the tiger
language to MIPS assembly code (executable by the [[http://pages.cs.wisc.edu/~larus/spim.html][SPIM]] simulator) It
is expected that students attend the live lab session as we will also
evaluate and monitor the progress of the students.

Please follow the instructions given below for ensuring easy
evaluation of your lab assignments and project.

** Instructions.

1. Create a /private git repository/ on [[https://gitlab.com][gitlab]] whose name is your
   rollnumber-compiler-lab. For example suppose you are James Bond,
   then your roll number in IIT Palakkad would be ~111801007~ in which
   case your repository would be named ~111801007-compiler-lab~. All
   your lab assignments needs to be committed to this repository.

2. The repository should contain the following.

   - ~README~ :: This file should contain your name and roll number at
                 the top. Besides it can contain any other data you might
		 want to say about your project.

   - ~CHANGELOG~ :: A weekly entry on what was done for the evaluation
                    that week (there is going to be an evaluation
                    every week). Some times the assignment deadline
                    might be longer than a week (if it is more
                    difficult) or you might have missed the weekly
                    deadline. there still should be an entry for that
                    week in the CHANGELOG.

   - ~src~ :: The directory where the source code of your compiler
              will reside.

3. Give /read-only/ access to the instructors and the TA in charge of
   this course. We can thus get hold of your assignments easily.

4. Every week just before the weekly session (which is on wednessday)
   you should do the following.

   - Solve the assignment of previous week. 
   - Add an entry in the ~CHANGELOG~ file about what you have done.
   - Commit all the changes so far and add a tag.

6. Solve the assignment problem and ensure that all the source files
   required to build and run your code are within the directory
   created above. Follow good coding practices by making commits that
   are small and meaningful.

7. Keep pushing your changes to gitlab. When you are ready with the
   assignment of the week, you should tag your repository using the
   [[https://git-scm.com/book/en/v2/Git-Basics-Tagging][git tag command]]. The
   tag should be ~YYYY-MM-DD~ when the correponding assignment is
   ~YYYY-MM-DD-Some-Title.md~.

8. You /do not/ have to explicitly submit the assignments, just ensure
   that your submission related work is merged to your ~master~ branch
   and pushed (along with the tags) to your gitlab account. We will
   get the assignments form there.

** Our workflow (for the curious).

If you are curious to know how we get your assignment please read
through. Knowing it is /not/ essential to successful completion of
weekly assignment or the project.

We have a private repository on gitlab where each of your repositories
are [[https://git-scm.com/book/en/v2/Git-Tools-Submodules][submodules]]. At the end of each deadline, we update the submodules
thereby getting all your assignments in one go. So if you have missed
on some commits, all you need to do is push them to your repository
and inform the TA.
