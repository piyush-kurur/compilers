#+TITLE: Lab 3: Language with variables (Deadline 3 March 2022).


The goal of this assignment is to build a compiler for a subset of the
tiger language which is little more than a desk calculator; a language
with variables and assignment. The compiler takes the source language
and converts it to MIPS assembly which can be executed by the SPIM
simulator.


* The subset of tiger.

1. Assume that the only supported types in integers.

2. The language consists of statements

   #+begin_example
   program = list of stmt
   stmt =  identifier := expresion
        |  print expression

   #+end_example

   where print is a key word of the language. I have not given the
   definition of expression but make sure that you have all reasonable
   operations of integers implemented like plus, mul, etc.

   Here is an example program in this version of tiger

   #+begin_example
   y := 2
   x := 40 + y
   print x + y
   #+end_example

* Handling variables.

  Consider the code generation as two stage process.

  - Code generation :: Convert from the source language to MIPS with
    the registers being defined by ~Temp.temp~ (See
    [[#sec-temps][Section on Temps]). I.e convert the source language
    into `(string, Temp.temp) MIPS.stmt list`.  Make use of your
    target/MIPS.org.  This will be your /Intermediate Representation/
    (IR).

    #+begin_example sml

    structure IR : sig
      type inst = (string, Temp.temp) MIPS.inst
      type stmt = (string, Temp.temp) MIPS.stmt
      type prog = stmt list
      val ppInst : inst -> string
      val ppStmt : stmt -> string
      val pp     : prog -> string
    end = struct
      (* complete this *)
    end
    #+end_example


  - Register Allocation :: Do a simple register allocation by assigning
    MIPS registers greedily to temps. If you run out of registers just
    flag an error.

  - Pretty printing IR :: Make sure that you have a way of printing
    the IR that you generate in the code generation phase. Otherwise
    you will have difficulty in debugging your code generator.

  - User variables :: You will have to maintain an /Environment/ which
    is a mapping from user variables to ~Temp.temp~. As you see new
    variables, you will need to update the environment by "allocating"
    a ~Temp.temp~ for your newly born variable. At any point of time
    the temp assigned to a user variable will keep track of the value
    of that variable.

    Things that can simplify this

    1. You may use [[https://www.classes.cs.uchicago.edu/archive/2015/spring/22620-1/atom-sig.html][Atom.atom]] to represent your variables.  [[https://www.classes.cs.uchicago.edu/archive/2015/spring/22620-1/atom-sig.html][Atom.atom]]
       can be thought of as a string type that supports fast comparison
       and order checking.

    2. For the environment you want to maintain a key value lookup table
       where the key is [[https://www.classes.cs.uchicago.edu/archive/2015/spring/22620-1/atom-sig.html][Atom.atom]] and value is ~Temp.temp~. You can make
       use of the [[https://www.classes.cs.uchicago.edu/archive/2015/spring/22620-1/ord-map-sig.html][AtomMap]] structure for this.

** Code generation.

   The MIPS assembly language supports essentially instructions of the
   kind ~a := b ∘ c~ and ~a := b ∘ constant~ where a,b,c are registers
   and c is a constant. See
   <https://www.dsi.unive.it/~gasparetto/materials/MIPS_Instruction_Set.pdf>.

   Write a helper function that is has the following signature

   #+begin_example sml

   val assign : Env.t -> Temp.temp -> expr -> IR.prog
   val print  : Env.t -> Temp.temp -> IR.prog

   #+end_example

   The assign would pattern match on the expr argument and generate
   the code freely allocating temps.


** Register allocation.

   For this assignment, we will use a rather simple minded register
   allocation where allocation is done greedly. This would mean that
   often we will run out of registers and will not be able to produce
   actual executable. In real compilers, there will be phases which
   will be more clever with register allocation and when it finds that
   temps cannot be allocated registers, would /spill/ them to main
   memory of the machine. In our case we can have these spilled
   variables in the data section of the MIPS assembly. However, in
   actual programs we might have to spill the variables into the stack
   or even the heap.

   But before we implement such an allocator, some "optimisations" in
   the code generation phase can drastically reduce the number of temps use
   thereby reducing the chances of running out of registers.

   #+begin_example
   v := a * a + b * b + 2 * a * b
   #+end_example

   A simple minded code generator would generate code as follows

   #+begin_example
   t₀ := a  * a
   t₁ := b  * b
   t₂ := t₀ + t₁
   t₃ := 2  * a
   t₄ := t₃ * b
   t₅ := t₂ + t₄
   v  := t₅
   #+end_example

   You will notice that many of the temporaries can be reused (for
   example t₃ can be t₀ and t₄ can be t₁ and so on. Find a way to
   generate code more optimally.

    #+begin_example
   t₀ := a  * a
   t₁ := b  * b
   t₂ := t₀ + t₁
   t₀ := 2  * a
   t₁ := t₀ * b
   v  := t₂ + t₁
   #+end_example


* Temps as variables.
#+lable:sec-temps

We will use the ~Temp~ structure to manage temporary variables which
essentially maintains a counter and increments it whenever a new temp
is generated. The signature of the Temp structure is given below.


#+begin_example sml
  signature TEMP =
    sig
       type temp
       val newtemp    : unit -> temp
       val tempToString : temp -> string
    end

  structure Temp :> TEMP = struct

     type temp  = int (* 2ʷ many variables on a w-sized machine *)
		      (* you can use IntInf.int if you want unbounded *)

     val nextTemp       = ref 0 (* Keep track of how many temps have been allocated *)
     fun newtemp  _     = (* complete this *)
     fun tempToString t = (* complete this *)
  end
#+end_example

The ~tempToString~ function will be used to pretty print your IR.

** Printing the IR, i.e. MIPS with  Temp.temp as registers

While debugging your code generator, you would want to print the MIPS
assembly program which makes use of the ~Temp.temps~ as the registers.
We show how this can be achieved by mininmal change in your MIPS
structure defined in the previous assignment.

Rework the pretty printing as follows.

1. Define the following functions ~MIPS.mapI~ and ~MIPS.mapS~ with the
   following signature.

   #+begin_example sml

   structure MIPS = struct
     ...
     val mapI : ('l -> 'lp) -> ('t -> 'tp) -> ('l,'t) inst -> ('lp,'tp) inst
     val mapS : ('l -> 'lp) -> ('t -> 'tp) -> ('l,'t) stmt -> ('lp,'tp) inst

     val prInst : (string, string) inst -> string
     val prStmt : (string, string) stmt -> string

     val regToString : reg -> string

     ...

   #+end_example

2. Use the `Temp.tempToString` together with `prInst` and `prStmt` to
   pretty print the MIPS instruction that uses Temp.temp

* Atom and AtomMap

  Here is the API for Atoms and AtomMaps for quick reference.
  For documentation of ~Atom~ and ~AtomMap~ refer to the URL

  <https://www.classes.cs.uchicago.edu/archive/2015/spring/22620-1/smlnj-lib.html>

#+begin_src sml :results value code
open Atom
open AtomMap
#+end_src

#+RESULTS:
#+begin_src sml
opening Atom
  type atom
  val atom : string -> atom
  val atom' : substring -> atom
  val toString : atom -> string
  val same : atom * atom -> bool
  val sameAtom : atom * atom -> bool
  val compare : atom * atom -> order
  val lexCompare : atom * atom -> order
  val hash : atom -> word
opening AtomMap
  structure Key :
    sig
      type ord_key = atom
      val compare : ord_key * ord_key -> order
    end
  type 'a map
  val empty : 'a map
  val isEmpty : 'a map -> bool
  val singleton : Key.ord_key * 'a -> 'a map
  val insert : 'a map * Key.ord_key * 'a -> 'a map
  val insert' : (Key.ord_key * 'a) * 'a map -> 'a map
  val find : 'a map * Key.ord_key -> 'a option
  val lookup : 'a map * Key.ord_key -> 'a
  val inDomain : 'a map * Key.ord_key -> bool
  val remove : 'a map * Key.ord_key -> 'a map * 'a
  val first : 'a map -> 'a option
  val firsti : 'a map -> (Key.ord_key * 'a) option
  val numItems : 'a map -> int
  val listItems : 'a map -> 'a list
  val listItemsi : 'a map -> (Key.ord_key * 'a) list
  val listKeys : 'a map -> Key.ord_key list
  val collate : ('a * 'a -> order) -> 'a map * 'a map -> order
  val unionWith : ('a * 'a -> 'a) -> 'a map * 'a map -> 'a map
  val unionWithi : (Key.ord_key * 'a * 'a -> 'a) -> 'a map * 'a map -> 'a map
  val intersectWith : ('a * 'b -> 'c) -> 'a map * 'b map -> 'c map
  val intersectWithi : (Key.ord_key * 'a * 'b -> 'c)
                       -> 'a map * 'b map -> 'c map
  val mergeWith : ('a option * 'b option -> 'c option)
                  -> 'a map * 'b map -> 'c map
  val mergeWithi : (Key.ord_key * 'a option * 'b option -> 'c option)
                   -> 'a map * 'b map -> 'c map
  val app : ('a -> unit) -> 'a map -> unit
  val appi : (Key.ord_key * 'a -> unit) -> 'a map -> unit
  val map : ('a -> 'b) -> 'a map -> 'b map
  val mapi : (Key.ord_key * 'a -> 'b) -> 'a map -> 'b map
  val foldl : ('a * 'b -> 'b) -> 'b -> 'a map -> 'b
  val foldli : (Key.ord_key * 'a * 'b -> 'b) -> 'b -> 'a map -> 'b
  val foldr : ('a * 'b -> 'b) -> 'b -> 'a map -> 'b
  val foldri : (Key.ord_key * 'a * 'b -> 'b) -> 'b -> 'a map -> 'b
  val filter : ('a -> bool) -> 'a map -> 'a map
  val filteri : (Key.ord_key * 'a -> bool) -> 'a map -> 'a map
  val mapPartial : ('a -> 'b option) -> 'a map -> 'b map
  val mapPartiali : (Key.ord_key * 'a -> 'b option) -> 'a map -> 'b map
  val exists : ('a -> bool) -> 'a map -> bool
  val existsi : (Key.ord_key * 'a -> bool) -> 'a map -> bool
  val all : ('a -> bool) -> 'a map -> bool
  val alli : (Key.ord_key * 'a -> bool) -> 'a map -> bool
#+end_src
