#+TITLE: Lab 6 : Compiling Tiger to Tree intermediate representation.

* Task.

In this task you need to compile tiger code into Tree IR.

1. The tiger code has functions but Tree IR does not have it.
   Functions should become labels.

2. Compiling Conditionals should be done with care.

3. Modify the ~tc~ compiler to print the tree IR

#+BEGIN_EXAMPLE
tc --ir foo.tig # This should print the tree IR representation
                # associated with the tiger program in foo.tig

#+END_EXAMPLE

For ease of implementation we can assume the following restriction
on the tiger language.


1. All variables are of type integers (This means you can avoid the
   type checking phase).

2. Boolean variables do not exist but boolean values can be created
   (for loops and conditionals) involving relational operators over
   integers.

3. Functions can be recursive but are not nested (You can avoid
   complicated things like static links etc).

#+BEGIN_EXAMPLE



let x := 45 in
let y := x + 10 in
    x * y

The above tiger code should become the following tree code

compile

ESEQ ( MOVE (TEMP t₁ , CONST 45),

       ESEQ ( TEMP t₂ , (BINOP (PLUS, TEMP t₁, CONST 10))
            , BINOP (MUL, TEMP t₁, TEMP t₂)
            )
      )

(*

variables, constants, plus operator, let

tiger-simple

e = const
  | variable
  | e₁ + e₂
  | let x = e₁ in e₂

*)

#+END_EXAMPLE

#+BEGIN_SRC sml
exception Unsupported of string

signature TEMP =
  sig
     type label
     type temp
     val newlabel : unit -> label  (* generate a new label *)
     val newtemp  : unit -> temp
  end

structure Temp :> TEMP = struct


   type label = int (* 2⁶⁴ = ∞ many variables *)
   type temp  = int

   (* you can use IntInf.int *)

   val nextLabel = ref 0
   val nextTemp  = ref 0

   fun newlabel _ = raise Unsupported "newlable"
   fun newtemp  _ = raise Unsupported "newtemp"


end


(* Environment of 'a values

i.e. a Finite map from strings to 'a

*)
signature ENV =
  sig
     type 'a t
     val lookup  : string -> 'a t -> 'a option
     val update  : string * 'a
                 -> 'a t
                 -> 'a t
  end

structure Env : ENV = struct
  ...
  (* Use the standard SML library Maps for implementing
    environment
  *)

end

structure Tree =
  struct

     datatype binop = PLUS | MINUS | MUL | DIV
     datatype relop = EQ | NE | LT | GT | LE | GE

     datatype expr = CONST of int
                   | NAME  of Temp.label
                       (* tiger level : functions, destinations for conditionals etc *)
                       (* processor : assembly language address *)
                   | TEMP  of Temp.temp
                       (* tiger level : variables unbounded number *)
                       (* processor level : processor registers    *)
                   | BINOP of binop * expr * expr
                   | MEM   of expr   (* processor level : an address's content *)
                   | CALL  of expr * expr list
                                     (* func (arg1, arg2 ....) *)
                   | ESEQ of stmt * expr
     and stmt = MOVE  of expr * expr
              | EXP   of expr
              | JUMP  of expr * Temp.label list
              | CJUMP of relop * expr * expr * Temp.label * Temp.label
              (* CJUMP rop e1 e2 l1 l2  = if e1 rop e2 then goto l1 else goto l2 *)
              | SEQ of stmt * stmt
              | LABEL of Temp.label


end

structure Tiger = struct

  datatype expr = C of int
                | V of string
                | PLUS of exp * exp
                | LET of string * exp * exp

end
(*

val compile : Temp.temp Env.t ->  Tiger.exp -> Tree.expr

*)

fun plusc env e1 e2 = let val ce1 = compile e1
                          val ce2 = compile e2
                      in Tree.BINOP (PLUS,ce1, ce2)
and varc  env x       =
and letc  env x e1 e2 = ...
and compile env (e : Tiger.expr) =
    case e of
      C x            => Tree.CONST x
    | PLUS (e1,e2)   => plus env e1 e2
    | V x            => varc env x
    | LET  (x,e1,e2) => letc env x e1 e2

    (*
      1. Allocate a new temp t₁ for x

      2. compile e1 to get ce1

      3. let env' = update env (x,t₁)

      the result is ESEQ ( MOVE t₁ , ce1)
                         , compile env' e2)


    *)
raise Unsupported "LET"

if ( cond1 && cond2 ) then c1 else c2

if cond1 || cond2  then c1 else c2


#+END_SRC

#+RESULTS:
#+begin_example
exception Unsupported of string
signature TEMP =
  sig
    type label
    type temp
    val newlabel : unit -> label
    val newtemp : unit -> temp
  end
structure Temp : TEMP
structure Tree :
  sig
    datatype binop = DIV | MINUS | MUL | PLUS
    datatype relop = EQ | GE | GT | LE | LT | NE
    datatype expr
      = BINOP of binop * expr * expr
      | CALL of expr * expr list
      | CONST of int
      | ESEQ of stmt * expr
      | MEM of expr
      | NAME of Temp.label
      | TEMP of Temp.temp
    datatype stmt
      = CJUMP of relop * expr * expr * Temp.label * Temp.label
      | EXP of expr
      | JUMP of expr * Temp.label list
      | LABEL of Temp.label
      | MOVE of expr * expr
      | SEQ of stmt * stmt
  end
structure Tiger :
  sig
    datatype expr
      = C of int | LET of string * exp * exp | PLUS of exp * exp | V of string
  end
val compile = fn : exp -> 'a
#+end_example
