# Wrapping up

In the last assignment we converted tiger code into the intermediate
language called tree for which a canonical form was obtained. The
final code generator is written by translating this to MIPS (with
temporaries in place of registers. In our case these temps will be
finally allocated on the stack. An instruction like `x := y + z` will
become a two loads for getting `y` and `z` into registers say `a0` and
`a1`, the operation `a3 := a0 + a1` followed by a store of `a3` into
the location for `x`.


```sh
#
# instruction x := y + z
# Becomes the following mips code.
#
# We assume that x is at offset 0 of sp, y is at
# offset 4 (32-bit integer) of sp and z is at offset 8
#
#

lw $a0, 4($sp)      # load y into the register a0
lw $a1, 8($sp)      # load z into the register a1
add $a3, $a0, $a1   # do the addition with result in a3
sw $a3,$sp          # store a3 into the location for x (on the stack)

```
