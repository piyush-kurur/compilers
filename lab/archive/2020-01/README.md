# Laboratory problems

The aim of this lab sessions is to get you familiarised with tools
that are required to build a compiler. One of the outcomes of this lab
is that you build a small compiler for the tiger language.  The weekly
sessions will be designed so as to help you achieve this goal. A lab
session will have one or more of the following.

1. A _weekly assignment_ which will illustrate some aspect of building a
   compiler.

2. A _project component_ that will add up towards the compiler for the
   tiger language that would be your course project.

3. An evaluation of the weekly component give in the previous week and
   a progress monitoring on the compiler project.
   
4. A demo of a tool or some clarification/doubt clearing session if
   required.

Please follow the instructions given below for ensuring easy
evaluation of your lab assignments and project.

## Instructions.

1. Create a _private git repository_ on bitbucket whose name is your
   rollnumber-compiler-lab. For example suppose you are James Bond,
   then your roll number in IIT Palakkad would be `111601007` in which
   case your repository would be named `111601007-compiler-lab`. All
   your lab assignments needs to be committed to this repository.

2. Add a `README` file at the top level of your repository with your
   details like Name and Roll number, and a sub-directory called
   `project` which will contain your tiger compilers source.

3. Give _read-only_ access to the instructors and the TA in charge of
   this course. We can thus get hold of your assignments easily.

4. While the tasks related to project should go to the `project`
   sub-directory that you have created, each of the weekly assignment
   should be in a directory of its own, with the name
   `YYYY-MM-DD-Title`, where `YYYY-MM-DD` is the date of upload of the
   assignment (not the deadline of the assignment). I will be
   uploading the problem statement with the file name
   `YYYY-MM-DD-Title.md` in this directory.

5. Solve the assignment problem and ensure that all the source files
   required to build and run your code are within the directory
   created above. Follow good coding practices by making commits that
   are small and meaningful.

6. Keep pushing your changes to bitbucket. When you are ready with the
   assignment of the week, you should tag your repository using the
   [`git tag` command][tag-command]. The tag should be `YYYY-MM-DD`
   when the correponding assignment is `YYYY-MM-DD-Some-Title.md`.

7. You _do not_ have to explicitly submit the assignments, just ensure
   that your submission related work is merged to your `master` branch
   and pushed (along with the tags) to your bitbucket account. We will
   get the assignments form there.

## Our workflow (for the curious).

If you are curious to know how we get your assignment please read
through. Knowing it is _not_ essential to successful completion of
weekly assignment and/or project. 

We have a private repository on bitbucket where each of your
repositories are [submodules]. At the end of each deadline, we update
the submodules thereby getting all your assignments in one go. So if
you have missed on some commits, all you need to do is push them to
your repository and inform the TA.

## List of assignments for Jan-2020 session.

[submodules]: <https://git-scm.com/book/en/v2/Git-Tools-Submodules> "Git submodules"
[tag-command]: <https://git-scm.com/book/en/v2/Git-Basics-Tagging> "Git tagging"
